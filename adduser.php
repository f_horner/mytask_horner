<?php
	require_once('inc/config.php');
	require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
	<head>
		<?php require_once('tpl/head.php'); ?>
  	</head>
  	<body class="userlist-body">
		<div class="off-canvas-wrapper">
			<?php require_once('tpl/header.php'); ?>
			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="title">NOUVEAU SOLDAT</h1>
					<!-- On envoie les infos à updateuser.php en post pour + de sécurité -->
					<form method="post" action="updateuser.php" class="small-12 medium-6 collumn">
			            <label>NOM</label>
			            <input type="text" name="name"/>
						<label>E-MAIL</label>
			            <input type="text" name="email"/>
						<label>MOT DE PASSE</label>
			            <input type="password" name="password"/>
			            <input type="submit" value="AJOUTER" class="button"/>
			        </form>
				</div>
			</main>
			<?php require_once('tpl/footer.php');?>
		</div>
	</body>
</html>