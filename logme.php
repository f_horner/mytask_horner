<?php
	require_once('inc/config.php');

	if(isset($_SESSION['userid']))
		header('Location:index.php');

	//Si le mot de passe et l'email sont bons, on va nous retourner l'id de la session
	$query = $db -> prepare('SELECT id FROM user WHERE email = ? AND password = ?');
	$query -> execute(array($_POST['email'], $_POST['password']));
	$result = $query -> fetch();

	//Si différent de null alors l'id existe et ouvre la session du user
	if($result != null) {
		$_SESSION['userid'] = $result['id'];
		header('Location:index.php');
	}

	else
		header('Location:login.php');
?>
