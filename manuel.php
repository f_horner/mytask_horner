<?php
	require_once('inc/config.php');
	require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  	<head>
		<?php require_once('tpl/head.php'); ?>
  	</head>
  	<body class="manuel-body">
		<div class="off-canvas-wrapper">
			<?php require_once('tpl/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="title">MANUEL UTILISATEUR</h1>
					<div class="manuel">
						<h3>Bienvenue sur myTask</h3>
						<p>Chers utilisateurs, merci d'avoir choisi <a href="index.php" target="_blank">mytask.com</a> comme gestionnaire de troupes et de missions. C'est avec un grand plaisir que nous vous invitons à lire la notice ci-dessous pour de plus amples informations sur le fonctionnement du site. Bonne lecture!</p>
						
						<h3>Login</h3>
						<p><img class="mode-img" alt="Page Login" src="assets/img/login.png"></p>
						<p>Lorsque vous vous êtes connectés à <a href="index.php" target="_blank">mytask.com</a>, la première chose que vous avez aperçue a été la page "Login". Sur cette page, vous devez insérer votre adresse e-mail et votre mot de passe. Une fois saisies, cliquez sur le bouton "Login", votre session est alors établie.</p>

						<h3>Missions</h3>
						<p><img class="mode-img" alt="Page Missions" src="assets/img/tasklist.png"></p>
						<p>Une fois connectés, vous êtes redirigés sur la page "Missions". C'est ici que vous pouvez consulter les missions en cours, leur créateur, leur échéance,... Vous êtes en mesure de créer de nouvelles missions en cliquant sur le bouton <img alt="+" src="assets/img/bouton.png" width="40"> ou encore modifier une déjà existante. Pour cela il faut cliquer sur les icônes <i class="fa fa-pencil" aria-hidden="true"></i><i class="fa fa-times" aria-hidden="true"></i><i class="fa fa-check" aria-hidden="true"></i>. L'icône en forme de crayon <i class="fa fa-pencil" aria-hidden="true"></i> permet de modifier les informations de votre mission comme la description, la personne assignée ou encore l'échéance. Viens ensuite la croix rouge <i class="fa fa-times" aria-hidden="true"></i> qui permet de supprimer une mission. Et le vue vert <i class="fa fa-check" aria-hidden="true"></i> qui change le statut de votre mission de "open" à "close". La création et la modification d'une mission seront traîtées plus tard, merci de continuer la lecture.</p>

						<h3>Troupes</h3>
						<p><img class="mode-img" alt="Page Troupes" src="assets/img/userlist.png"></p>
						<p>En cliquant sur le menu "Troupes", vous arriverez sur la page vous permettant de consulter vos utilisateurs. Vous êtes en mesure de créer de nouveaux utilisateurs en cliquant sur le bouton <img alt="+" src="assets/img/bouton.png" width="40"> ou encore en modifier un déjà existante. Pour cela il faut cliquer sur les icônes <i class="fa fa-pencil" aria-hidden="true"></i><i class="fa fa-times" aria-hidden="true"></i>. L'icône en forme de crayon <i class="fa fa-pencil" aria-hidden="true"></i> permet de modifier les informations de votre utilisateur comme son nom, son email ou encore son mot de passe. La croix rouge <i class="fa fa-times" aria-hidden="true"></i> permet de supprimer un utilisateur. La création et la modification d'un utilisateur seront traîtées plus tard, merci de continuer la lecture.</p>

						<h3>Menu latéral et supérieur</h3>
						<p><img class="mode-img" alt="Page Troupes" src="assets/img/menu.png"></p>
						<p>Deux menus sont à dispositions l'un latéral et l'autre supérieur. Le supérieur n'est disponible que sur la version large et medium du site. Sur un écran plus réduit à l'image d'un smartphone, le menu latéral prend tout son sens. Ce dernier est permanent et est accessible en cliquant sur le bouton <img class="mode-img" alt="Icône accéder au menu latéral" src="assets/img/bouton_menu.png" width="20">. Toujours dans le menu, il vous est possible d'accéder à votre profil ("root" dans l'exemple) ou encore de vous déconnecter en cliquant sur "Logout". Pour accéder à ces options, survolez votre photo de profil avec votre souris en haut à drtoie.</p>

						<h3>Création et modification d'une mission</h3>
						<p><img class="mode-img" alt="Création/modification d'une mission" src="assets/img/nouv_mission.png"></p>
						<p>Les pages de création et d'édition d'une mission sont identiques à quelques différences près. Pour créer une nouvelle mission, il faut cliquer sur le bouton <img alt="+" src="assets/img/bouton.png" width="40"> de la page "Missions". Tandis que pour en éditer une, il faut cliquer sur le crayon <i class="fa fa-pencil" aria-hidden="true"></i>, les informations de la mission seront alors chargées et vous pourrez les éditer.

						Les champs restent les mêmes dans les deux cas. Le premier "Description" vous permet d'insérer un bref descriptif. Ensuite, vient le champ "Priorité" qui vous met à disposition une échelle allant de 1 à 5 (1 étant urgent et 5 à repousser). Vous avez la possibilité par la suite d'insérer une date dans le champ "Échéance" au format américain à savoir mm/jj/aaaa. Le dernier champ vous permet d'assigner la mission à un membre enregistré dans la communauté <a href="index.php">mytask.com</a>. Cliquez sur "Ajouter" ou "Modifier" en fonction de votre manipulation initiale pour valider le tout.

						<h3>Création et modification d'un utilisateur</h3>
						<p><img class="mode-img" alt="Création/modification d'un utilisateur" src="assets/img/nouv_user.png"></p>
						<p>Les pages de création et d'édition d'un utilisateur sont identiques à quelques différences près. Pour créer un nouvel utilisateur, il faut cliquer sur le bouton <img alt="+" src="assets/img/bouton.png" width="40"> de la page "Missions". Tandis que pour en éditer un, il faut cliquer sur le crayon <i class="fa fa-pencil" aria-hidden="true"></i>, les informations de l'utilisateur seront alors chargées et vous pourrez les éditer.

						Les champs restent les mêmes dans les deux cas. "Nom" vous permet d'insérer le nom de votre utilisateur. Ensuite, vient le champ "E-mail" dans lequel vous pouvez enregistrer votre adresse email, notez que le @ est nécessaire à l'enregistrer de votre utilisateur. Il vous est pour finir possible de créer un mot de passe dans le champ "Mot de passe". Cliquez sur "Ajouter" ou "Modifier" en fonction de votre manipulation initiale pour valider le tout.</p>

						<h3>About</h3>
						<p><img class="mode-img" alt="Page About" src="assets/img/about.png"></p>
						<p>La page "About" vous permet d'accéder à ce manuel ainsi que des projets annexes effectués lors du semestre du cours ICommunication. Pour cela, il faut cliquer sur les <a href="">liens hypertextes</a>.</p>

						<h3>Remerciements et informations suplémentaires</h3>
						<p>Merci d'avoir lu cette petite notice, en cas de questions ou d'incertitudes, merci de contacter l'administrateur du site:</p>
						<ul class="manuel-list">
							<li class="manuel-list-item">Horner Frédéric</li>
							<li class="manuel-list-item">Tél. 079 666 88 68</li>
							<li class="manuel-list-item">E-mail: frederic.horner@bluewin.ch</li>
						</ul>
					</div>
				</div>
			</main>
			<?php require('tpl/footer.php'); ?>
		</div>
  </body>
</html>
