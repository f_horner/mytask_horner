<?php
	require_once('inc/config.php');

	//Si le user est bien dans la db, alors on le laisse accéder à index.php
	if(isset($_SESSION['userid']))
		header('Location:index.php');
?>

<!doctype html>
<html class="no-js" lang="fr">
  	<head>
		<?php require_once('tpl/head.php');?>
  	</head>
  	<body class="background-login">
		<main class="row columns small-12 large-4">
			<!-- On envoie les infos à logme.php qui utilise la méthode post pour + de sécurité -->
			<form method="post" action="logme.php">
				<div class="login-item">
					<label for="email">E-MAIL</label>
					<input type="text" name="email" id="email"/>
					<label for="password">MOT DE PASSE</label>
					<input type="password" name="password" id="password"/>
					<input type="submit" value="LOGIN" class="button"/>
				</div>
			</form>
		</main>
		<?php require_once('inc/script.php'); ?>
  	</body>
</html>