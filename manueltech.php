<?php
	require_once('inc/config.php');
	require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  	<head>
		<?php require_once('tpl/head.php'); ?>
  	</head>
  	<body class="manuel-body">
		<div class="off-canvas-wrapper">
			<?php require_once('tpl/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="title">MANUEL TECHNIQUE</h1>
					<div class="manuel">
						<h3>Bienvenue sur myTask</h3>
						<p>Chers dévellopeurs, nous vous remercions d'ores et déjà pour votre travail sur <a href="index.php" target="_blank">mytask.com</a>. Le site regroupe de nombreux fichiers importants à comprendre et surtout à maîtriser afin de garantir le bon fonctionnement du site. Les éléments présentés ci-dessous sont les plus utiles ce qui signifie pas que les éléments non mentionnés ne sont pas de mêmes! Merci de lire avec attention cette notice.<br/>
						Conseil: Toutes les pages ci-dessous ont été commentées, il est plus que recommandé de consulter les fichiers simultanément à la lecture de cette notice, cela dans le but de faciliter la compréhension. Il est aussi conseillé de lire <a class="aboutlist-item" href="manuel.php" target="_blank">le manuel utilisateur</a> qui est complémentaire à ce manuel technique.</p>

						<h3>Base de données</h3>
						<p><img class="mode-img" alt="Base de données" src="assets/img/adminer.png"></p>
						<p>Notre base de données est gérée à l'aide de la plateforme <a href="https://www.adminer.org/en/" target="_blank">Adminer</a>. Celle-ci est comparable à PHPMyAdmin mais offre de plus amples avantages. Notre base de données abrégée db se nomme tasklist. Elle possède deux tables à savoir une pour les tâches/missions à effectuer et une autre pour les utilisateurs. Ces champs sont divers et doivent être consultés avant toutes modifications du code liés à la db! Pour s'y connecter, merci de contacter votre administrateur qui pourra vous founrnir les informations de connexions.</p>

						<h3>Foundation</h3>
						<p>Le framework <a href="http://foundation.zurb.com/" target="_blank">Foundation</a> est utilisé au sein de ce site. Nous faisons confiance à cet outil car ce dernier propose des avantages non-négligeables à l'image du responsive ou encore de librairie préconçues. Par exemple, le offcanvas utilisé derrière le menu en fait partie.</p>

						<h3>BitBucket</h3>
						<p>Nous sauvons nos fichiers sur la plateforme <a href="https://bitbucket.org" target="_blank">BitBucket</a> afin de limiter la perte de fichiers. Cette plateforme possède des commandes dites "git" à utiliser depuis le terminal de votre machine. En voici un aperçu, merci de consulter le site pour plus de précisions.</p>
						<ul>
							<li>git init</li>
							<li>git remote add ...</li>
							<li>git add -A</li>
							<li>git commit -m 'mon premier git vide'</li>
							<li>git push origin master</li>
							<li>git pull</li>
						</ul>

						<h3>Arborescence</h3>
						<p><img class="mode-img" alt="Arborescence des fichiers" src="assets/img/arborescence.png"></p>
						<p>Il y a de nombreux dossiers et fichiers importants à comprendre pour manipuler ce site. Voici un bref descriptif de leur utilité et de leur contenu. Pous de plus amples précisions, merci de consulter les codes de ces fichiers ou de contacter votre administrateur.</p></p>
						
						<h4>.guit</h4>
						<p>Le dossier ".guit" permet d'assurer la sauvegarde des fichiers sur le BitBucket. A chaque fois que vous serez amenés à faire votre "git add-A" et les instructions qui suivent, c'est ici que tout va se dérouler.</p>

						<h4>assets</h4>
						<p>Le dossier "assets" regroupe toutes les ressources multimédias du site telles que le CSS, le jquery, les images ou encore les polices d'écritures.</p>

						<h4>bower_components</h4>
						<p>Ce dossier est créé à chaque nouveau projet Foundation. Les librairies du framework y sont stockées.</p>

						<h4>inc</h4>
						<p>Le dossier "inc" est le plus important de cet arborescence, il est nécessaire de le manipuler avec précautions. Ce dossier contient trois fichiers PHP. Ils servent à gérer respectivement la connexion à la db, les liaisons avec les scripts jquery et la sécurité du site.</p>
						
						<h5>config.php</h5>
						<p>Ce fichier permet de se connecter avec la db et est appelé en début de chaque page du site.</p>
						<p>Exemple avec une db, un administrateur et un mot de passe fictifs.</p>
						<p><img class="mode-img" alt="config.php" src="assets/img/config.png"></p>
						
						<h5>script.php</h5>
						<p>"script.php" permet de lier nos librairies jquery avec le site. Ce fichier est appelé au sein du fichier "footer.php".</p>
						<p><img class="mode-img" alt="script.php" src="assets/img/script.png"></p>
						
						<h5>security.php</h5>
						<p>Fichier important, il est appelé sur chaque page et va vérifier si l'utilisateur courant a le droit de naviguer. Sinon, on le redirige vers la page login.</p>
						<p><img class="mode-img" alt="security.php" src="assets/img/security.png"></p>

						<h4>node_modules</h4>
						<p>Ici sont enregistrés tous les modules jquery que vous souhaiter intégrer à votre site. Des éléments présents dans ce dossier sont appelés dans le fichier "script.php".</p>

						<h4>projets</h4>
						<p>Il regroupe les éléments présents sur la page "About", vous trouverez ici les deux projets à savoir <a class="aboutlist-item" href="projets/responsive/responsive.html" target="_blank">le site responsive</a> et <a class="aboutlist-item" href="projets/vador/main.html" target="_blank">le CV de Dark Vador</a>.</p>

						<h4>scss</h4>
						<p>C'est dans le dossier "scss" que vous créerez votre design et plus précisemment dans le dossier "modules". Chaque fichiers SCCS doit comporter la même dénommination à savoir "_nomdufichier.scss". Vous pourrez ensuite insérer votre code à l'intérieur en respectant la même syntaxe que le CSS. Le fichier "_settings.scss" comporte toutes les paramètres CSS d'origine, prenez garde en le manipulant. Vos modifications terminées, n'oubliez pas d'importer votre document (si ce n'est pas déjà fait) au sein du fichier "app.scss" dans les dernières lignes du code. Faites ensuite un "foundation watch" au sein de votre terminal, le répertoire pointé préalablement doit être votre dossier Foundation. Rafraichissez votre page et le design prendra forme.</p>
						<p>Remarques: selon votre naviguateur, il est nécessaire de vider le cache afin de voir les dernières modifications effectuées sur votre site.</p>
						<p><img class="mode-img" alt="Rajout d'un document dans app.scss" src="assets/img/app.png"></p>

						<h4>tpl</h4>
						<p>Comme son nom l'indique, "tpl" regroupe les templates communs à toutes les pages du site.</p>

						<h5>footer.php</h5>
						<p>Le pied-de-page contient l'appel des scripts pour faire fonctionner le Javascript.</p>
						<p><img class="mode-img" alt="footer.php" src="assets/img/footer.png"></p>

						<h5>head.php</h5>
						<p>"head.php" contient le titre du site, l'utf-8 ou encore les fonts utilisées. C'est ici que se situe l'appel au fichier "app.css" qui regroupe les fichiers SCSS compilés.</p>
						<p><img class="mode-img" alt="head.php" src="assets/img/head.png"></p>

						<h5>header.php</h5>
						<p>Le fichier "header.php" est important et contient l'élément le plus visible du site à savoir le menu. C'est ici que le menu vertical et supérieur sont définis. Une partie de code PHP et MySQL y sont aussi présentes et permettent d'afficher l'image de l'utilisateur en haut à droite. Cette image doit être préalablement créée et nommée comme ceci "profil_IdUser.jpg".</p>
						<p>Voici une portion du code de header.php.</p>
						<p><img class="mode-img" alt="header.php" src="assets/img/header.png"></p>
						
						<h4>Les fichiers à la racine</h4>
						<h5>login.php</h5>
						<p>Ce formulaire est la première chose que l'utilisateur est en mesure de voir lorsqu'il souhaite se connecter à <a href="index.php" target="_blank">mytask.com</a>. Il lui est demandé d'insérer son e-mail ainsi que son mot de passe afin de s'authentifier.</p>
						<p><img class="mode-img" alt="Page Login" src="assets/img/login.png"></p>
						<p>Une fois les informations saisies, celles-ci sont transmises à "logme.php" qui va se charger de les traîter. Afin de faciliter la naviguation, l'utilisateur n'est pas obligé de repasser par cela si sa session est déjà ouverte.</p>
						<p>Voici une portion du code de "login.php".</p>
						<p><img class="mode-img" alt="login.php" src="assets/img/login_code.png"></p>

						<h5>logme.php</h5>
						<p>"logme.php" va recevoir l'e-mail et le mot de passe transmis par le précédent formulaire. Il va ensuite vérifier qu'un user enregistré dans la db possède bien ces informations. Si oui on continue à "index.php" sinon, on revient sur "login.php".</p>
						<p>Voici une portion du code de "logme.php".</p>
						<p><img class="mode-img" alt="logme.php" src="assets/img/logme.png"></p>

						<h5>logout.php</h5>
						<p>Comme son nom l'indique, "logout.php va déconnecter la session du user et va le rediriger vers la page "login.php"</p>
						<p><img class="mode-img" alt="logout.php" src="assets/img/logout.png"></p>						

						<h5>index.php et users.php</h5>
						<p>Une fois connecté, l'utilisateur est directement redirigé sur cette page ayant pour titre "Missions". C'est ici qu'ils peuvent consulter les tâches/missions en cours, leur créateur, leur échéance,... Ils sont en mesure de créer de nouvelles tâches/missions en cliquant sur le bouton <img alt="+" src="assets/img/bouton.png" width="40"> ou encore modifier une déjà existante. Pour cela il doivent cliquer sur les icônes <i class="fa fa-pencil" aria-hidden="true"></i><i class="fa fa-times" aria-hidden="true"></i><i class="fa fa-check" aria-hidden="true"></i>. L'icône en forme de crayon <i class="fa fa-pencil" aria-hidden="true"></i><i class="fa fa-check" aria-hidden="true"></i> permet de modifier les informations de la tâche/mission comme la description, la personne assignée ou encore l'échéance. Viens ensuite la croix rouge <i class="fa fa-times" aria-hidden="true"></i> qui permet de la supprimer. Et le vue vert <i class="fa fa-check" aria-hidden="true"></i> qui change le statut de la tâche/mission de "open" à "close".</p>
						<p>Voici la page index.php</p>
						<p><img class="mode-img" alt="index.php" src="assets/img/tasklist.png"></p>

						<p>Cette page regroupe de nombreuses requêtes MySQL et des codes PHP nécessaires pour aller chercher les champs de la db et les afficher. Les boutons <img alt="+" src="assets/img/bouton.png" width="40"> et <i class="fa fa-pencil" aria-hidden="true"></i> font appel respectivement aux formulaires "add.php" et "edit.php". Ce dernier fournit en plus au formulaire l'id de la tâche/mission déjà existante.</p> 
						<p>"users.php" fonctionnent de la même manière que la page "index.php". La logique est la même hormis le fait qu'on ne puisse pas changer de statut à un utilisateur, les autres boutons sont identiques.
						<p>Voici une portion du code de index.php.</p>
						<p><img class="mode-img" alt="index.php" src="assets/img/index.png"></p>

						<h5>add.php et adduser.php</h5>
						<p>Pour accéder à ses formulaires, l'utilisateur a dû cliquer sur le bouton <img alt="+" src="assets/img/bouton.png" width="40"> des pages "index.php" ou "users.php". L'utilisateur est ensuite invité à remplir les champs et d'y insérer correctement les informations requises. En cas de non-ĉonformité, il ne pourra valider son enregistrement et un petit message d'erreur lui dira ce qui ne va pas. Pour connaître la nature et la signification exacte des champs, merci de consulter la db ou encore <a class="aboutlist-item" href="manuel.php" target="_blank">le manuel utilisateur</a>. Une fois rempli, l'utilisateur va cliquer sur "Ajouter",</p>
						<p>Voici la page "add.php".</p>
						<p><img class="mode-img" alt="add.php" src="assets/img/nouv_mission.png"></p>
						<p>Ces formulaires sont identiques sur le plan de la logique et permettent tous deux d'ajouter de nouveaux éléments dans la base de données en faisant appel à d'autres fichiers nommés "update.php" et "updateuser.php".</p>
						<p>Voici une portion du code de add.php.</p>
						<p><img class="mode-img" alt="add.php" src="assets/img/add.png"></p>

						<h5>edit.php et edituser.php</h5>
						<p>Comme dit plus haut, l'utilisateur a dû cliquer sur le bouton <i class="fa fa-pencil" aria-hidden="true"></i> pour modifier un élément existant. Lors du clique, l'id est alors transmis et les informations de l'élément présentes dans la db remplissent les champs des formulaires "edit.php" ou encore "edituser.php". De même, ces deux fichiers sont quasiment identiques hormis leurs champs qui diffèrent. Ils appellent aussi tous deux les fichiers "update.php" et "updateuser.php".</p>
						<p>Voici une portion du code de edit.php.</p>
						<p><img class="mode-img" alt="edit.php" src="assets/img/edit.png"></p>
						
						<h5>update.php et updateuser.php</h5>
						<p>Ces deux fichiers sont ici pour traîter les informations saisies dans les quatre formulaires présentés précédemment (add,edit,..). Ils font la différence entre une nouvelle entrée ou une modification d'élément déjà existant. Tout cela fonctionne grâce à une condition, y a t-il oui ou non un id faisant référence à un élément déjà présent dans la db? Si oui alors on fait un "update" des champs, autrement on fait un "insert".</p>
						<p>Voici une portion du code de update.php</p>
						<p><img class="mode-img" alt="update.php" src="assets/img/update.png"></p>

						<h5>delete.php et deleteuser.php</h5>
						<p>A l'instar des doubles précédents, ces fichiers fonctionnent de la même manière mais agissent sur deux tables différentes. L'utilisateur a la possibilité de supprimer un élément en cliquant sur le bouton <i class="fa fa-times" aria-hidden="true"></i>. Une fois cliqué, l'id de l'élément est transmis à "delete.php" ou "deleteuser.php" qui se chargeront de le supprimer de la db.</p>
						<p>Voici le code de delete.php</p>
						<p><img class="mode-img" alt="delete.php" src="assets/img/delete.png"></p>

						<h5>done.php</h5>
						<p>En cliquant sur le bouton <i class="fa fa-check" aria-hidden="true"></i>, l'utilisateur va transmettre l'id de la tâche/mission au fichier "done.php". C'est lui qui va regarder à quelle tâche/mission appartient cet id et va updater son statut de "open" à "close".</p>
						<p><img class="mode-img" alt="done.php" src="assets/img/done.png"></p>

						<h5>about.php</h5>
						<p>"about.php" est l'une des pages les plus simple du site. Elle ne regroupe que quelques liens hypertextes et ne contient pas d'interactions avec la db.</p>

						<h3>Remerciements et informations suplémentaires</h3>
						<p>Merci d'avoir lu cette petite notice, en cas de questions ou d'incertitudes, merci de contacter l'administrateur du site:</p>
						<ul class="manuel-list">
							<li class="manuel-list-item">Horner Frédéric</li>
							<li class="manuel-list-item">Tél. 079 666 88 68</li>
							<li class="manuel-list-item">E-mail: frederic.horner@bluewin.ch</li>
						</ul>
					</div>
				</div>
			</main>
			<?php require('tpl/footer.php'); ?>
		</div>
  </body>
</html>
