<?php
	require_once('inc/config.php');
	require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  	<head>
		<?php require_once('tpl/head.php'); ?>
  	</head>
  	<body class="tasklist-body">
		<div class="off-canvas-wrapper">
			<?php require_once('tpl/header.php'); ?>
			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="title">MISSIONS</h1>
					<ul class="tasklist">
						<!-- il faut mettre hide pour que cela disparaisse au format small -->
						<li class="hide-for-small-only tasklist-header">
							<span class="tasklist-item-id">
								ID
							</span>
							<span class="tasklist-item-priority">
								PRIORITÉ
							</span>
							<span class="tasklist-item-description">
								DESCRIPTION
							</span>
							<span class="tasklist-item-creator">
								CRÉÉ PAR
							</span>
							<span class="tasklist-item-assignee">
								ASSIGNÉ À
							</span>
							<span class="tasklist-item-due">
								ÉCHÉANCE
							</span>
							<span class="tasklist-item-actions">
								ACTIONS
							</span>
						</li>
							<?php
								//On sélectionne tous les champs de nos 2 tables task et user
	          					$query = $db -> prepare('SELECT task.id, description, created_at, due_at, priority, status, creator.id as creator_id, creator.name as creator_name, assignee.id as assignee_id, assignee.name as assignee_name FROM task INNER JOIN user as creator on created_by = creator.id LEFT JOIN user as finishor on done_by = finishor.id INNER JOIN user as assignee on assigned_to = assignee.id');

								$query -> execute();
					        	while($data = $query -> fetch()):
					        ?>
					    <!-- On affiche les champs de notre db -->
						<li class="tasklist-item<?php if($data['status'] == 'close'): ?> tasklist-item-close<?php endif; ?>">
		            		<span class="tasklist-item-id">
		              			<?php echo $data['id']; ?>
		            		</span>
							<span class="hide-for-small-only tasklist-item-priority">
		              			<?php echo $data['priority']; ?>
		            		</span>
				            <span class="tasklist-item-description">
				              	<?php echo $data['description']; ?>
				            </span>
							<span class="hide-for-small-only tasklist-item-creator">
								<?php echo $data['creator_name']; ?>
							</span>
							<span class="hide-for-small-only tasklist-item-assignee">
								<?php echo $data['assignee_name']; ?>
							</span>
				            <span class="hide-for-small-only tasklist-item-due">
				              <?php echo $data['due_at']; ?>
				            </span>
			            	<span class="tasklist-item-actions">
								<a href="edit.php?id=<?php echo $data['id']; ?>">
			                		<i class="fa fa-pencil" aria-hidden="true"></i>
				              	</a>
				              	<a href="#" data-delete="<?php echo $data['id']; ?>">
				                	<i class="fa fa-times" aria-hidden="true"></i>
				              	</a>
								<a href="#" data-done="<?php echo $data['id']; ?>">
				                	<i class="fa fa-check" aria-hidden="true"></i>
				              	</a>
				            </span>
			          	</li>
			        	<?php endwhile; ?>
					</ul>
					<!-- Filtre et tri
					Mon filtre/tri ne fonctionnent pas, je mets quand même ce que j'ai trouvé sur internet et ce que j'ai testé durant le projet. Si je ne le mets pas en commentaires, ils font planter le site.
					
					//Liens pour les filtres
					<a href="?filtre=all">Toutes les tâches</a>
					<a href="?filtre=mytask">Mes tâches</a>
					<a href="?filtre=close">Tâches archivées</a>
					
					//Les filtres avec le PHP et MySql nécessaire pour filtrer
					if(isset($_GET['filtre'])) {
						switch($_GET['filtre']) {
							case 'mytask':
								$filtre = 'WHERE assigned_to = '.$_SESSION['userid'];
								break;
							case 'close':
								$filtre = 'WHERE status ="close"';
								break;
							default:
								$filtre = 'WHERE 1';
								break;
						}
					}

					//Liens pour les tris
					<a href="?tri=created_by_up">Créateur</a>
					<a href="?tri=created_by_down">Créateur</a>
					<a href="?tri=statut_up">Statut</a>
					<a href="?tri=statut_down">Statut</a>
					
					//Les tris avec le PHP et MySQL
					$tri =	null;
					if(isset($_GET['tri'])) {
						switch($_GET['tri']) {
							case 'created_by_up':
								$tri = 'ORDER BY created_by ASC';
								break;
							case 'created_by_down':
								$tri = 'ORDER BY created_by DESC';
								break;
							case 'statut_up':
								$order = 'ORDER BY status ASC';
								break;
							case 'statut_down':
								$order = 'ORDER BY status DESC';
								break;
							...
						}
					}
					-->
					<a class="btn-add" href="add.php">+</a>
				</div>
			</main>
			<?php require('tpl/footer.php'); ?>
		</div>
  </body>
</html>
