<?php
	require_once('inc/config.php');
	require_once('inc/security.php');

	//On regarde si le user est déjà existant, si non on en refait un nouveau
	if(isset($_POST['id'])) {
		$query = $db -> prepare('UPDATE user SET name =	?, email = ?, password = ? WHERE id = ?');
		$query -> execute(array($_POST['name'], $_POST['email'], $_POST['password'], $_POST['id']));
	}

	else {
		$query = $db -> prepare('INSERT INTO user(name, email, password) VALUES(?, ?, ?)');
		$query -> execute(array($_POST['name'], $_POST['email'], $_POST['password']));
	}

	header('Location:users.php');
?>
