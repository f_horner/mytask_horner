<?php
	require_once('inc/config.php');
	require_once('inc/security.php');
?>
<header class="top row expanded">
	<div class="top-bar">
		<div class="top-bar-title">
			<button class="menu-icon" type="button" data-toggle="offCanvas"></button>
			<a href="index.php" class="top-bar-menusup">MyTask.com</a>
			<!-- Il ne faut pas oublier le hide pour cacher les menus sur un affichage small -->
			<a href="index.php" class="hide-for-small-only top-bar-menu">MISSIONS</a>
			<a href="users.php" class="hide-for-small-only top-bar-menu">TROUPES</a>
			<a href="about.php" class="hide-for-small-only top-bar-menu">ABOUT</a>
		</div>
		<div class="top-bar-right">
			<!-- On sélectionne les infos du user grâce à son id de session -->
			<?php
				$query =	$db -> prepare('SELECT * FROM user WHERE id = ?');
				$query -> execute(array($_SESSION['userid']));
				$data = $query -> fetch();
			?>
			<ul class="hide-for-small-only dropdown menu" data-dropdown-menu>
				<li>
					<img src="assets/img/profil_<?php echo $_SESSION['userid'];?>.jpg" class="header-picture" />
					<ul class="menu">
						<!-- On affiche son nom, lorsqu'il clique dessus, il peut éditer ses informations -->
						<li><a href="edituser.php?id=<?php echo $_SESSION['userid']; ?>" class="vertical-menu-item"><?php echo $data['name']; ?></a></li>
						<li><a href="logout.php"  class="vertical-menu-item">Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</header>

<nav class="nav off-canvas-absolute position-left" id="offCanvas" data-off-canvas>
	<ul class="vertical menu">
		<li><img src="assets/img/offcanvas.jpg"></li>
		<li><a href="" class="hide"></a></li>
		<li><a href="index.php" class="vertical-menu-item">Missions</a></li>
		<li><a href="add.php" class="vertical-menu-item">Nouvelle mission</a></li>
		<li><a href="users.php" class="vertical-menu-item">Troupes</a></li>
		<li><a href="adduser.php" class="vertical-menu-item">Nouveau soldat</a></li>
		<li><a href="about.php" class="vertical-menu-item">About</a></li>
		<li><a href="logout.php" class="vertical-menu-item">Logout</a></li>
	</ul>
</nav>
