<?php
	require_once('inc/config.php');
	require_once('inc/security.php');
	//On change le statut de la tâche de"open" à "close" grâce à l'id transmis depuis index
	$query = $db -> prepare('UPDATE task SET done_by = ?, status = "close" WHERE id = ?');
	$query -> execute(array($_SESSION['userid'], $_GET['id']));
?>
