<?php
	require_once('inc/config.php');
	require_once('inc/security.php');
	//On déconnecte le user courant et on retourne à la page login.php
	unset($_SESSION['userid']);
	header('Location:login.php');
?>
