<?php
	require_once('inc/config.php');
	require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  	<head>
		<?php require_once('tpl/head.php'); ?>
  	</head>
  	<body class="userlist-body">
		<div class="off-canvas-wrapper">
			<?php require_once('tpl/header.php');	?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="title">TROUPES</h1>
					<ul class="userlist">
						<!-- il faut mettre hide pour que cela disparaisse au format small -->
						<li class="hide-for-small-only userlist-header">
							<span class="userlist-item-id">
								ID
							</span>
							<span class="userlist-item-username">
								UTILISATEUR
							</span>
							<span class="userlist-item-email">
								EMAIL
							</span>
							<span class="userlist-item-actions">
								ACTIONS
							</span>
						</li>
						<?php
							//On sélectionne tous les champs de notre table user
				        	$query = $db -> query('SELECT * FROM user');
				          	while($data = $query -> fetch()):
				        ?>
				    	<!-- On affiche les champs de notre db -->
						<li class="userlist-item">
				            <span class="userlist-item-id">
				            	<?php echo $data['id']; ?>
				            </span>
				            <span class="userlist-item-username">
				              	<?php echo $data['name']; ?>
				            </span>		
				            <span class="hide-for-small-only userlist-item-email">
				              	<?php echo $data['email']; ?>
				            </span>
				            <span class="userlist-item-actions">										<a href="edituser.php?id=<?php echo $data['id']; ?>">
				                	<i class="fa fa-pencil" aria-hidden="true"></i>
				              	</a>
				              	<a href="#" data-deleteuser="<?php echo $data['id']; ?>">
				                	<i class="fa fa-times" aria-hidden="true"></i>
				              	</a>
				            </span>
	          			</li>
	          			<?php endwhile; ?>
					</ul>
					<a class="btn-add" href="adduser.php">+</a>
				</div>
				
			</main>

			<?php require_once('tpl/footer.php');	?>
		</div>
  	</body>
</html>
