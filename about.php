<?php
	require_once('inc/config.php');
	require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  	<head>
		<?php require_once('tpl/head.php'); ?>
  	</head>
  	<body class="about-body">
		<div class="off-canvas-wrapper">
			<?php require_once('tpl/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="title">ABOUT</h1>
					<div class="about">
						<h3>Le projet MyTask</h3>
						<p>Ce site est un projet qui a été mené au sein du cours de ICommunication. <br/>Il doit comporter de nombreux éléments web tels que du HTML, CSS, PHP et MySQL.<br/>Afin de guider les utilisateurs et les futurs développeurs, des manuels ont alors été rédigés.</p>
						<p>
						<a class="aboutlist-item" href="manuel.php" target="_blank">Manuel utilisateur</a><br/>
						<a class="aboutlist-item" href="manueltech.php" target="_blank">Manuel technique</a>
						</p>

						<h3>Les projets de semestre</h3>
						<p>Ce semestre a eu pour objectif de nous initier au monde du web et du développement.<br/>HTML, CSS, responsive,... tous ces outils nous ont permis de faire des exerices extraordinaires.<br/>En voici une partie, faite durant les cours et reprenant les points les plus importants de ce semestre.
						</p>

						<p>Nos connaissances en HTML et CSS ont été démontrées dans un CV.<br/> Ce CV a été fait sur le thème de Dark Vador pour plus de fantaisies.</p>
						<p><a class="aboutlist-item" href="projets/vador/main.html" target="_blank">Le CV de Dark Vador</a></p>
						
						<p>Plus tard, nous avons vu le responsive et son utilité dans le monde d'aujourd'hui.<br/> Nous avons fait un examen traitant de cela et avons dû adapter le tout avec des medias queries.</p>	
						<p><a class="aboutlist-item" href="projets/responsive/responsive.html" target="_blank">Exercice sur le reponsive de l'examen</a></p>

						<p>Nous avons aussi vu le PHP et MySQL et les avons utilisés pour mettre en place MyTask.</p>	
					</div>
				</div>
			</main>
			<?php require('tpl/footer.php'); ?>
		</div>
  </body>
</html>
