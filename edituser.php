<?php
	require_once('inc/config.php');
	require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
	<head>
		<?php require_once('tpl/head.php'); ?>
  	</head>
  	<body class="userlist-body">
		<div class="off-canvas-wrapper">
			<?php require_once('tpl/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="title">Editer un utilisateur</h1>
					<!-- On va rechercher l'id transmis depuis users.php dans la db et on sélectionne tous les champs -->
					<?php
						$query = $db -> prepare('SELECT * FROM user WHERE id = ?');
	        			$query -> execute(array($_GET['id']));
	        			$data = $query -> fetch();
					?>
					<!-- On envoie les informations en post à updateuser.php -->
					<form method="post" action="updateuser.php" class="small-12 medium-6 collumn">
						<input name="id" type="hidden" value="<?php echo $_GET['id']; ?>"/>
			            <label>NOM</label>
			            <input type="text" name="name" value="<?php echo $data['name']; ?>"/>
						<label>ADRESSE E-MAIL</label>
            			<input type="email" name="email" value="<?php echo $data['email']; ?>"/>
						<label>MOT DE PASSE</label>
            			<input type="password" name="password" value="<?php echo $data['password']; ?>"/>
            			<input type="submit" value="MODIFIER" class="button"/>
	        		</form>
				</div>
			</main>

			<?php require_once('tpl/footer.php'); ?>
		</div>
  	</body>
</html>
