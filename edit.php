<?php
	require_once('inc/config.php');
	require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  	<head>
		<?php require_once('tpl/head.php'); ?>
  	</head>
  	<body class="tasklist-body">
		<div class="off-canvas-wrapper">
			<?php require_once('tpl/header.php'); ?>
			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="title">Editer une tâche</h1>
					<!-- On va rechercher l'id transmis depuis index.php dans la db et on sélectionne tous les champs -->
					<?php
						$query = $db -> prepare('SELECT * FROM task WHERE id = ?');
		        		$query -> execute(array($_GET['id']));
		        		$data = $query -> fetch();
					?>
					<!-- On renvoie les informations en post à update.php -->
					<form method="post" action="update.php" class="small-12 medium-6 collumn">
						<input name="id" type="hidden" value="<?php echo $_GET['id']; ?>"/>
			            <!-- On affiche les valeurs des champs propre à l'id -->
			            <label>Description</label>
			            <textarea name="description" rows="6"><?php echo $data['description']; ?></textarea>
			            <label>Priorité</label>
			            <select name="priority">
							<option value="<?php echo $data['priority']; ?>"><?php echo $data['priority']; ?></option>
				            <?php for($i = 1; $i <= 5; $i++): ?>
				           	<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				            <?php endfor; ?>
            			</select>
            			<label>Délai</label>
            			<input type="date" name="due_at" value="<?php echo $data['due_at']; ?>"/>
						<label>Assignée à</label>
						<select name="assigned_to">
							<?php
								$query = $db -> query('SELECT * FROM user');
								while($data =	$query -> fetch()):
							?>
							<option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>
							<?php
								endwhile;
							?>
            			</select>
            			<input type="submit" value="Modifier" class="button"/>
	        		</form>
				</div>
			</main>
			<?php require_once('tpl/footer.php'); ?>
		</div>
	</body>
</html>
