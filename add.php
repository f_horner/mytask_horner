<?php
	require_once('inc/config.php');
	require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
	<head>
		<?php require_once('tpl/head.php');?>
  	</head>
  	<body class="tasklist-body">
		<div class="off-canvas-wrapper">
			<?php require_once('tpl/header.php');?>
			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="title">NOUVELLE MISSION</h1>
					<!-- On envoie les infos à update.php en post pour + de sécurité -->
					<form method="post" action="update.php" class="small-12 medium-6 collumn">
		            	<label>DESCRIPTION</label>
			            <!-- Le rows="4" agrandit le bloc textarea sur la hauteur -->
			            <textarea name="description" rows="4"></textarea>
			            <label>PRIORITÉ</label>
			            <select name="priority">
			              <!-- Menu déroulant, les possibilités sont créées en itérant dans une boucle while -->
			              <?php for($i = 1; $i <= 5; $i++): ?>
			                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
			              <?php endfor; ?>
			            </select>
			            <label>ÉCHÉANCE</label>
			            <input type="date" name="due_at"/>
						<label>ASSIGNÉ À</label>
						<select name="assigned_to">
							<!-- On va chercher dans la db tous les utilisateurs, on peut ensuite leur attribuer une tâche -->
							<?php
								$query = $db -> query('SELECT * FROM user');
								while($data =	$query -> fetch()):
							?>
							<option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>
							<?php
								endwhile;
							?>
			            </select>
			            <input type="submit" value="AJOUTER" class="button"/>
	        		</form>
				</div>
			</main>

			<?php require_once('tpl/footer.php'); ?>
		</div>
	</body>
</html>
