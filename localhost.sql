-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `due_at` datetime NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `priority` tinyint(1) NOT NULL DEFAULT '1',
  `status` enum('open','close') CHARACTER SET utf8 NOT NULL DEFAULT 'open',
  `done_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `task` (`id`, `description`, `created_at`, `created_by`, `due_at`, `assigned_to`, `priority`, `status`, `done_by`) VALUES
(25,	'non',	'2017-06-24 07:37:02',	9,	'1996-05-08 00:00:00',	9,	1,	'close',	4),
(26,	'Hello',	'2017-06-24 07:37:31',	4,	'1996-02-02 00:00:00',	9,	3,	'open',	NULL),
(30,	'sadfasdf',	'2017-06-25 12:33:18',	9,	'0044-01-22 00:00:00',	9,	4,	'open',	NULL),
(29,	'sdafasfd',	'2017-06-24 10:04:22',	9,	'1555-02-02 00:00:00',	9,	3,	'open',	NULL),
(31,	'je veux aller au li9t',	'2017-06-25 14:41:16',	9,	'2014-02-11 00:00:00',	9,	3,	'open',	NULL);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `name`, `email`, `password`) VALUES
(9,	'root',	'root',	'root'),
(12,	'sdfasdf',	'asdfasf',	'asdf');

-- 2017-06-26 08:41:21
